/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.funcao;

import com.mycompany.controller.ClienteDAO;
import com.mycompany.view.JFCliente;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author diegomoraes
 */
public class FuncCliente {

    ClienteDAO clienteDAO = new ClienteDAO();

    //LIMPAR CAMPOS
    public void limparCampos(JTextField id, JTextField nome, JTextField sobrenome, JFormattedTextField cpf_cnpj,
            JFormattedTextField datanasc, JTextField email, JFormattedTextField cep, JTextField logradouro, JTextField bairro,
            JTextField cidade, JTextField estado, JFormattedTextField fixo, JFormattedTextField celular) {

        id.setText(null);
        nome.setText(null);
        sobrenome.setText(null);
        cpf_cnpj.setText(null);
        datanasc.setText(null);
        email.setText(null);
        cep.setText(null);
        logradouro.setText(null);
        bairro.setText(null);
        cidade.setText(null);
        estado.setText(null);
        fixo.setText(null);
        celular.setText(null);
    }

//Validação de Campos Vazios
    public boolean verificarCampos(JTextField nome, JTextField sobrenome, JFormattedTextField cpf_cnpj,
            JFormattedTextField datanasc, JTextField email, JFormattedTextField cep, JTextField logradouro, JTextField bairro,
            JTextField cidade, JTextField estado, JFormattedTextField fixo, JFormattedTextField celular, JFrame jfcliente) {

      if (nome.getText().isEmpty()) {
            JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha o campo Nome");
            nome.requestFocus();
            return false;
        } else {
            if (sobrenome.getText().isEmpty()) {
                JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha o campo Sobrenome");
                sobrenome.requestFocus();
                return false;
            } else {
                if (cpf_cnpj.getText().equals("   .   .   -  ")||cpf_cnpj.getText().equals("  .   .   /    -  ")||cpf_cnpj.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha o CPF ou CNPJ");
                    cpf_cnpj.requestFocus();
                    return false;
                } else {
                    if (datanasc.getText().equals("  /  /    ")) {
                        JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha a Data de Nascimento");
                        datanasc.requestFocus();
                        return false;
                    } else {
                        if (email.getText().isEmpty()) {
                            JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha o e-mail");
                            email.requestFocus();
                            return false;
                        } else {
                            if (cep.getText().equals("     -   ")) {
                                JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha o CEP");
                                cep.requestFocus();
                                return false;
                            } else {
                                if (logradouro.getText().isEmpty()) {
                                    JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha o Logradouro");
                                    logradouro.requestFocus();
                                    return false;
                                } else {
                                    if (bairro.getText().isEmpty()) {
                                        JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha o Bairro");
                                        bairro.requestFocus();
                                        return false;
                                    } else {
                                        if (cidade.getText().isEmpty()) {
                                            JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha a cidade");
                                            cidade.requestFocus();
                                            return false;
                                        } else {
                                            if (estado.getText().isEmpty()) {
                                                JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha o Estado");
                                                estado.requestFocus();
                                                return false;
                                            } else {
                                                if (fixo.getText().equals("(  )    -    ")) {
                                                    JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha o Telefone Fixo");
                                                    fixo.requestFocus();
                                                    return false;
                                                } else {
                                                    if (celular.getText().equals("(  )     -    ")) {
                                                        JOptionPane.showMessageDialog(jfcliente, "Por favor, preencha o Celular");
                                                        celular.requestFocus();
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return true;

    }
    //ENVIO DE ID PARA EXIBIÇÃO NA ABA DE MANUTENÇÃO

    public void enviarDados(JTable tabCliente, JTextField id, JTextField nome, JTextField sobrenome, JFormattedTextField cpf_cnpj, JFormattedTextField datanasc,
            JTextField email, JFormattedTextField cep, JTextField logradouro, JTextField bairro, JTextField cidade,
            JTextField estado, JFormattedTextField fixo, JFormattedTextField celular, JRadioButton radcpf,
            JRadioButton radcnpj, JTabbedPane pagina, JFrame jfcliente) {

        //DECLARAÇÃO DO RESULTSET
        ResultSet rs = null;

        //ATRIBUIR VALOR DO ID DA JTABLE
        int linha = tabCliente.getSelectedRow();
        id.setText(tabCliente.getModel().getValueAt(linha, 0).toString());

        //BUSCA DADOS REFERENTES AO ID
        try {

            //RECEBE RESULTADO DA CONSULTA NO RESULTSET
            rs = clienteDAO.consultarId(Integer.valueOf(id.getText()), jfcliente);

            //ATRIBUI VALORES DO RESULTSET PARA CADA CAMPO
            nome.setText(rs.getString("cli_nome"));
            sobrenome.setText(rs.getString("cli_sobrenome"));
            cpf_cnpj.setText(rs.getString("cli_cpf_cnpj"));
            datanasc.setText(rs.getString("cli_datanasc"));
            email.setText(rs.getString("cli_email"));
            cep.setText(rs.getString("cli_cep"));
            logradouro.setText(rs.getString("cli_endereco"));
            bairro.setText(rs.getString("cli_bairro"));
            cidade.setText(rs.getString("cli_cidade"));
            estado.setText(rs.getString("cli_estado"));
            fixo.setText(rs.getString("cli_telfixo"));
            celular.setText(rs.getString("cli_celular"));

            //VERIFICA SE O TIPO É CPF
            if (rs.getInt("cli_tipo") == 0) {

                radcpf.setSelected(true);

                //ATRIBUI A MÁSCARA PARA CPF
                try {
                    MaskFormatter mask = new MaskFormatter("###.###.###-##");
                    cpf_cnpj.setFormatterFactory(new DefaultFormatterFactory(mask));
                } catch (ParseException ex) {
                    Logger.getLogger(JFCliente.class.getName()).log(Level.SEVERE, null, ex);
                }

                //ATRIBUI VALOR DO RESULTSET PARA O CPF
                CPF cpf = new CPF(rs.getString("cli_cpf_cnpj"));
                cpf_cnpj.setText(cpf.getCPF(true));

            } else {
                //SE NÃO FOR UM CPF, É CNPJ
                radcnpj.setSelected(true);

                //ATRIBUI A MÁSCARA PARA CNPJ
                try {
                    MaskFormatter mask = new MaskFormatter("##.###.###/####-##");
                    cpf_cnpj.setFormatterFactory(new DefaultFormatterFactory(mask));
                } catch (ParseException ex) {
                    Logger.getLogger(JFCliente.class.getName()).log(Level.SEVERE, null, ex);
                }

                //ATRIBUI VALOR DO RESULTSET PARA O CNPJ
                CNPJ cnpj = new CNPJ(rs.getString("cli_cpf_cnpj"));
                cpf_cnpj.setText(cnpj.getCNPJ(true));
            }

        } catch (SQLException ex) {
            Logger.getLogger(FuncCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        //MIGRA PARA A PÁGINA DE MANUTENÇÃO DE CLIENTE
        pagina.setSelectedIndex(1);

    }

//HABILITAR CAMPOS
    public void habilitarCampos(JTextField id, JTextField nome, JTextField sobrenome, JFormattedTextField cpf_cnpj,
            JFormattedTextField datanasc, JTextField email, JFormattedTextField cep, JTextField logradouro, JTextField bairro,
            JTextField cidade, JTextField estado, JFormattedTextField fixo, JFormattedTextField celular) {

        nome.setEnabled(true);
        sobrenome.setEnabled(true);
        cpf_cnpj.setEnabled(true);
        datanasc.setEnabled(true);
        email.setEnabled(true);
        cep.setEnabled(true);
        logradouro.setEnabled(true);
        bairro.setEnabled(true);
        cidade.setEnabled(true);
        estado.setEnabled(true);
        fixo.setEnabled(true);
        celular.setEnabled(true);

    }

//DESABILITAR CAMPOS
    public void desabilitarCampos(JTextField id, JTextField nome, JTextField sobrenome, JFormattedTextField cpf_cnpj,
            JFormattedTextField datanasc, JTextField email, JFormattedTextField cep, JTextField logradouro, JTextField bairro,
            JTextField cidade, JTextField estado, JFormattedTextField fixo, JFormattedTextField celular) {

        nome.setEnabled(false);
        sobrenome.setEnabled(false);
        cpf_cnpj.setEnabled(false);
        datanasc.setEnabled(false);
        email.setEnabled(false);
        cep.setEnabled(false);
        logradouro.setEnabled(false);
        bairro.setEnabled(false);
        cidade.setEnabled(false);
        estado.setEnabled(false);
        fixo.setEnabled(false);
        celular.setEnabled(false);

    }

//Botões - Ação de Inserção
    public void acaoCUD(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar) {

        inserir.setEnabled(false);
        editar.setEnabled(false);
        deletar.setEnabled(false);
        ok.setEnabled(true);
        cancelar.setEnabled(true);
    

    }

//Botões - Ação de Ir para tela de Edição
    public void acaoIrEdicao(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar) {

        inserir.setEnabled(false);
        editar.setEnabled(true);
        deletar.setEnabled(true);
        ok.setEnabled(false);
        cancelar.setEnabled(false);

    }

//Botões - Ação de Edição
    public void acaoEdicao(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar) {

        inserir.setEnabled(false);
        editar.setEnabled(false);
        deletar.setEnabled(false);
        ok.setEnabled(true);
        cancelar.setEnabled(true);

    }

//Botões - Ação de OK e Cancelar
    public void acaoOkCancelar(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar) {

        inserir.setEnabled(true);
        editar.setEnabled(false);
        deletar.setEnabled(false);
        ok.setEnabled(false);
        cancelar.setEnabled(false);

    }

}
