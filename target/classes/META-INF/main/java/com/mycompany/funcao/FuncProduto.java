/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.funcao;


import com.mycompany.controller.ProdutoDAO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author diegomoraes
 */
public class FuncProduto {

    ProdutoDAO produtoDAO = new ProdutoDAO();

    //LIMPAR CAMPOS
    public void limparCampos(JTextField id, JTextField marca, JTextField modelo, JComboBox tipo, JTextField descricao,
            JTextField custo, JTextField venda) {

        id.setText(null);
        marca.setText(null);
        modelo.setText(null);
        tipo.setSelectedIndex(0);
        descricao.setText(null);
        custo.setText(null);
        venda.setText(null);
       
    }

//VALIDAÇÃO DE CAMPOS VAZIOS
    public boolean verificarCampos(JTextField marca, JTextField modelo, JTextField descricao,
            JTextField custo, JTextField venda, JFrame jfproduto) {

      if (marca.getText().isEmpty()) {
            JOptionPane.showMessageDialog(jfproduto, "Por favor, preencha o campo Marca");
            marca.requestFocus();
            return false;
        } else {
            if (modelo.getText().isEmpty()) {
                JOptionPane.showMessageDialog(jfproduto, "Por favor, preencha o campo Modelo");
                modelo.requestFocus();
                return false;
            } else {
                if (descricao.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(jfproduto, "Por favor, preencha a descrição");
                    descricao.requestFocus();
                    return false;
                } else {
                    if (custo.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(jfproduto, "Por favor, preencha o custo");
                        custo.requestFocus();
                        return false;
                    } else {
                        if (venda.getText().isEmpty()) {
                            JOptionPane.showMessageDialog(jfproduto, "Por favor, preencha o e-mail");
                            venda.requestFocus();
                            return false;
                        } 
                    }
                }
            }
        }

        return true;

    }
    //ENVIO DE ID PARA EXIBIÇÃO NA ABA DE MANUTENÇÃO

    public void enviarDados(JTable tabProduto, JTextField id, JTextField marca, JTextField modelo, JComboBox tipo, 
            JTextField descricao, JTextField custo, JTextField venda, JTabbedPane pagina, JFrame jfproduto) {

        //DECLARAÇÃO DO RESULTSET
        ResultSet rs = null;

        //ATRIBUIR VALOR DO ID DA JTABLE
        int linha = tabProduto.getSelectedRow();
        id.setText(tabProduto.getModel().getValueAt(linha, 0).toString());

        //BUSCA DADOS REFERENTES AO ID
        try {

            //RECEBE RESULTADO DA CONSULTA NO RESULTSET
            rs = produtoDAO.consultarId(Integer.valueOf(id.getText()), jfproduto);

            //ATRIBUI VALORES DO RESULTSET PARA CADA CAMPO
            marca.setText(rs.getString("prod_marca"));
            modelo.setText(rs.getString("prod_modelo"));
            tipo.setSelectedIndex(Integer.valueOf(rs.getString("prod_tipo")));
            descricao.setText(rs.getString("prod_descricao"));
            custo.setText(String.valueOf(rs.getString("prod_custo")));
            venda.setText(String.valueOf(rs.getString("prod_venda")));
                    

        } catch (SQLException ex) {
            Logger.getLogger(FuncProduto.class.getName()).log(Level.SEVERE, null, ex);
        }
        //MIGRA PARA A PÁGINA DE MANUTENÇÃO DE PRODUTO
        pagina.setSelectedIndex(1);

    }

//HABILITAR CAMPOS
    public void habilitarCampos(JTextField marca, JTextField modelo, JComboBox tipo, JTextField descricao,
                                JTextField custo, JTextField venda) {

        marca.setEnabled(true);
        modelo.setEnabled(true);
        tipo.setEnabled(true);
        descricao.setEnabled(true);
        custo.setEnabled(true);
        venda.setEnabled(true);
      
    }

//DESABILITAR CAMPOS
    public void desabilitarCampos(JTextField marca, JTextField modelo, JComboBox tipo, JTextField descricao,
                                JTextField custo, JTextField venda) {

        marca.setEnabled(false);
        modelo.setEnabled(false);
        tipo.setEnabled(false);
        descricao.setEnabled(false);
        custo.setEnabled(false);
        venda.setEnabled(false);
        
    }

//BOTÕES - AÇÃO DE INSERÇÃO
    public void acaoCUD(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar) {

        inserir.setEnabled(false);
        editar.setEnabled(false);
        deletar.setEnabled(false);
        ok.setEnabled(true);
        cancelar.setEnabled(true);
    

    }

//BOTÕES - AÇÃO DE IR PARA TELA DE EDIÇÃO
    public void acaoIrEdicao(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar) {

        inserir.setEnabled(false);
        editar.setEnabled(true);
        deletar.setEnabled(true);
        ok.setEnabled(false);
        cancelar.setEnabled(false);

    }

//BOTÕES - AÇÃO DE EDIÇÃO
    public void acaoEdicao(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar) {

        inserir.setEnabled(false);
        editar.setEnabled(false);
        deletar.setEnabled(false);
        ok.setEnabled(true);
        cancelar.setEnabled(true);

    }

//BOTÕES - AÇÃO DE OK E CANCELAR
    public void acaoOkCancelar(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar) {

        inserir.setEnabled(true);
        editar.setEnabled(false);
        deletar.setEnabled(false);
        ok.setEnabled(false);
        cancelar.setEnabled(false);

    }

}
