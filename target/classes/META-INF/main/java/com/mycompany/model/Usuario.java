/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

/**
 *
 * @author diegomoraes
 */
public class Usuario {
    
    //Atributos da Classe
    private int usu_id;
    private String usu_nome;
    private String usu_senha;
    private int usu_perfil;
    
    //Métodos da Classe

    /**
     * @return the usu_id
     */
    public int getUsu_id() {
        return usu_id;
    }

    /**
     * @param usu_id the usu_id to set
     */
    public void setUsu_id(int usu_id) {
        this.usu_id = usu_id;
    }

    /**
     * @return the usu_nome
     */
    public String getUsu_nome() {
        return usu_nome;
    }

    /**
     * @param usu_nome the usu_nome to set
     */
    public void setUsu_nome(String usu_nome) {
        this.usu_nome = usu_nome;
    }

    /**
     * @return the usu_senha
     */
    public String getUsu_senha() {
        return usu_senha;
    }

    /**
     * @param usu_senha the usu_senha to set
     */
    public void setUsu_senha(String usu_senha) {
        this.usu_senha = usu_senha;
    }

    /**
     * @return the usu_perfil
     */
    public int getUsu_perfil() {
        return usu_perfil;
    }

    /**
     * @param usu_perfil the usu_perfil to set
     */
    public void setUsu_perfil(int usu_perfil) {
        this.usu_perfil = usu_perfil;
    }
    
    
    
    
}
