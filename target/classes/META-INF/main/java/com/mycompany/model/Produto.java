/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

/**
 *
 * @author diegomoraes
 */
public class Produto {
    
    //ATRIBUTOS DA CLASSE PRODUTO
    private int prod_id;
    private String prod_marca;
    private String prod_modelo;
    private int prod_tipo;
    private String prod_descricao;
    private Double prod_custo;
    private Double prod_venda;
    
    
    //MÉTODOS DA CLASSE PRODUTO
    /**
     * @return the prod_id
     */
    public int getProd_id() {
        return prod_id;
    }

    /**
     * @param prod_id the prod_id to set
     */
    public void setProd_id(int prod_id) {
        this.prod_id = prod_id;
    }

    /**
     * @return the prod_marca
     */
    public String getProd_marca() {
        return prod_marca;
    }

    /**
     * @param prod_marca the prod_marca to set
     */
    public void setProd_marca(String prod_marca) {
        this.prod_marca = prod_marca;
    }

    /**
     * @return the prod_modelo
     */
    public String getProd_modelo() {
        return prod_modelo;
    }

    /**
     * @param prod_modelo the prod_modelo to set
     */
    public void setProd_modelo(String prod_modelo) {
        this.prod_modelo = prod_modelo;
    }

    /**
     * @return the prod_tipo
     */
    public int getProd_tipo() {
        return prod_tipo;
    }

    /**
     * @param prod_tipo the prod_tipo to set
     */
    public void setProd_tipo(int prod_tipo) {
        this.prod_tipo = prod_tipo;
    }

    /**
     * @return the prod_descricao
     */
    public String getProd_descricao() {
        return prod_descricao;
    }

    /**
     * @param prod_descricao the prod_descricao to set
     */
    public void setProd_descricao(String prod_descricao) {
        this.prod_descricao = prod_descricao;
    }

    /**
     * @return the prod_custo
     */
    public Double getProd_custo() {
        return prod_custo;
    }

    /**
     * @param prod_custo the prod_custo to set
     */
    public void setProd_custo(Double prod_custo) {
        this.prod_custo = prod_custo;
    }

    /**
     * @return the prod_venda
     */
    public Double getProd_venda() {
        return prod_venda;
    }

    /**
     * @param prod_venda the prod_venda to set
     */
    public void setProd_venda(Double prod_venda) {
        this.prod_venda = prod_venda;
    }
    
   
}
