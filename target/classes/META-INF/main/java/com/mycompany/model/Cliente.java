/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.util.Date;

/**
 *
 * @author diegomoraes
 */
public class Cliente {

  
    //ATRIBUTOS
    private int cli_id;
    private int cli_tipo;
    private String cli_nome;
    private String cli_sobrenome;
    private String cli_cpf_cnpj;
    private String cli_datanasc;
    private String cli_email;
    private String cli_cep;
    private String cli_logradouro;
    private String cli_bairro;
    private String cli_cidade;
    private String cli_estado;
    private String cli_telfixo;
    private String cli_celular;

    
    //MÉTODOS
    /**
     * @return the cli_id
     */
    public int getCli_id() {
        return cli_id;
    }

    /**
     * @param cli_id the cli_id to set
     */
    public void setCli_id(int cli_id) {
        this.cli_id = cli_id;
    }
    
      /**
     * @return the cli_tipo
     */
    public int getCli_tipo() {
        return cli_tipo;
    }

    /**
     * @param cli_tipo the cli_tipo to set
     */
    public void setCli_tipo(int cli_tipo) {
        this.cli_tipo = cli_tipo;
    }

    /**
     * @return the cli_nome
     */
    public String getCli_nome() {
        return cli_nome;
    }

    /**
     * @param cli_nome the cli_nome to set
     */
    public void setCli_nome(String cli_nome) {
        this.cli_nome = cli_nome;
    }

    /**
     * @return the cli_sobrenome
     */
    public String getCli_sobrenome() {
        return cli_sobrenome;
    }

    /**
     * @param cli_sobrenome the cli_sobrenome to set
     */
    public void setCli_sobrenome(String cli_sobrenome) {
        this.cli_sobrenome = cli_sobrenome;
    }

    /**
     * @return the cli_cpf_cnpj
     */
    public String getCli_cpf_cnpj() {
        return cli_cpf_cnpj;
    }

    /**
     * @param cli_cpf_cnpj the cli_cpf_cnpj to set
     */
    public void setCli_cpf_cnpj(String cli_cpf_cnpj) {
        this.cli_cpf_cnpj = cli_cpf_cnpj;
    }

    /**
     * @return the cli_datanasc
     */
    public String getCli_datanasc() {
        return cli_datanasc;
    }

    /**
     * @param cli_datanasc the cli_datanasc to set
     */
    public void setCli_datanasc(String cli_datanasc) {
        this.cli_datanasc = cli_datanasc;
    }

    /**
     * @return the cli_email
     */
    public String getCli_email() {
        return cli_email;
    }

    /**
     * @param cli_email the cli_email to set
     */
    public void setCli_email(String cli_email) {
        this.cli_email = cli_email;
    }

    /**
     * @return the cli_cep
     */
    public String getCli_cep() {
        return cli_cep;
    }

    /**
     * @param cli_cep the cli_cep to set
     */
    public void setCli_cep(String cli_cep) {
        this.cli_cep = cli_cep;
    }

    /**
     * @return the cli_logradouro
     */
    public String getCli_logradouro() {
        return cli_logradouro;
    }

    /**
     * @param cli_logradouro the cli_logradouro to set
     */
    public void setCli_logradouro(String cli_logradouro) {
        this.cli_logradouro = cli_logradouro;
    }

    /**
     * @return the cli_bairro
     */
    public String getCli_bairro() {
        return cli_bairro;
    }

    /**
     * @param cli_bairro the cli_bairro to set
     */
    public void setCli_bairro(String cli_bairro) {
        this.cli_bairro = cli_bairro;
    }

    /**
     * @return the cli_cidade
     */
    public String getCli_cidade() {
        return cli_cidade;
    }

    /**
     * @param cli_cidade the cli_cidade to set
     */
    public void setCli_cidade(String cli_cidade) {
        this.cli_cidade = cli_cidade;
    }

    /**
     * @return the cli_estado
     */
    public String getCli_estado() {
        return cli_estado;
    }

    /**
     * @param cli_estado the cli_estado to set
     */
    public void setCli_estado(String cli_estado) {
        this.cli_estado = cli_estado;
    }

    /**
     * @return the cli_telfixo
     */
    public String getCli_telfixo() {
        return cli_telfixo;
    }

    /**
     * @param cli_telfixo the cli_telfixo to set
     */
    public void setCli_telfixo(String cli_telfixo) {
        this.cli_telfixo = cli_telfixo;
    }

    /**
     * @return the cli_celular
     */
    public String getCli_celular() {
        return cli_celular;
    }

    /**
     * @param cli_celular the cli_celular to set
     */
    public void setCli_celular(String cli_celular) {
        this.cli_celular = cli_celular;
    }
    
    
    
}
