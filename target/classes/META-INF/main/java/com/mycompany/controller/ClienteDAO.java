/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controller;

import com.mycompany.model.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author diegomoraes
 */
public class ClienteDAO {

    //ATRIBUTOS
    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
    
    
    //MÉTODO DE CONSULTA PARA TODOS OS REGISTROS
    public void consultarTodos(JTable tabcliente, JFrame jfcliente){
        
          String sql = "select cli_id as ID, cli_nome as Nome, cli_sobrenome as Sobrenome, cli_cpf_cnpj as CPF_CNPJ from Cliente";
        
        try {
            conexao=Conexao.conectar();
            pst=conexao.prepareStatement(sql);
            rs=pst.executeQuery();
            
            tabcliente.setModel(DbUtils.resultSetToTableModel(rs));
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfcliente, "Ocorreu um erro: "+e);
        }
        
        Conexao.desconector(conexao);
        
    }
    
    
     //MÉTODO DE CONSULTA POR NOME
    public void consultarNome(JTable tabclientevenda, JFrame jfvenda, JTextField busca){
        
          String sql = "select cli_id as ID, cli_nome as Nome, cli_cpf_cnpj as CPF_CNPJ from Cliente where cli_nome like ?";
        
        try {
            conexao=Conexao.conectar();
            pst=conexao.prepareStatement(sql);
            pst.setString(1, busca.getText() + "%");
            rs=pst.executeQuery();
            
            tabclientevenda.setModel(DbUtils.resultSetToTableModel(rs));
            tabclientevenda.getColumnModel().getColumn(0).setPreferredWidth(10);
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfvenda, "Ocorreu um erro: "+e);
        }
        
        Conexao.desconector(conexao);
        
    }
    
    
     //MÉTODO DE CONSULTA POR ID
    public ResultSet consultarId(int id, JFrame jfcliente) throws SQLException{
        
          String sql = "select * from Cliente where cli_id="+id;
        
        try {
            conexao=Conexao.conectar();
            pst=conexao.prepareStatement(sql);
            rs=pst.executeQuery();
            
            
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfcliente, "Ocorreu um erro: "+e);
            
            
        }
        if(rs.next()){
             return rs;
        }else{
          return rs=null;  
        }
       
       // Conexao.desconector(conexao);
        
        
    }

    //MÉTODO DE INSERÇÃO DE CLIENTE
    public void inserirCliente(Cliente cliente, JFrame jfcliente) {

        String sql = "insert into Cliente(cli_nome, cli_sobrenome, cli_datanasc, cli_endereco, cli_cidade, cli_estado,"
                + "cli_telfixo, cli_celular, cli_email, cli_cpf_cnpj, cli_cep, cli_bairro, cli_tipo) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try {

            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);

            pst.setString(1, cliente.getCli_nome());
            pst.setString(2, cliente.getCli_sobrenome());
            pst.setString(3, cliente.getCli_datanasc());
            pst.setString(4, cliente.getCli_logradouro());
            pst.setString(5, cliente.getCli_cidade());
            pst.setString(6, cliente.getCli_estado());
            pst.setString(7, cliente.getCli_telfixo());
            pst.setString(8, cliente.getCli_celular());
            pst.setString(9, cliente.getCli_email());
            pst.setString(10, cliente.getCli_cpf_cnpj());
            pst.setString(11, cliente.getCli_cep());
            pst.setString(12, cliente.getCli_bairro());
            pst.setInt(13, cliente.getCli_tipo());

            pst.execute();

            JOptionPane.showMessageDialog(jfcliente, "Cliente inserido com sucesso!");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfcliente, "Erro ao inserir cliente: " + e);
        }

        Conexao.desconector(conexao);

    }

    //MÉTODO DE ALTERAÇÃO DE CLIENTE
    public void alterarCliente(Cliente cliente, JFrame jfcliente) {

        String sql = "update Cliente set cli_nome=?, cli_sobrenome=?, cli_datanasc=?, cli_endereco=?, cli_cidade=?, cli_estado=?,"
                + "cli_telfixo=?, cli_celular=?, cli_email=?, cli_cpf_cnpj=?, cli_cep=?, cli_bairro=?, cli_tipo=? where cli_id=?";

        try {

            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);

            pst.setString(1, cliente.getCli_nome());
            pst.setString(2, cliente.getCli_sobrenome());
            pst.setString(3, cliente.getCli_datanasc());
            pst.setString(4, cliente.getCli_logradouro());
            pst.setString(5, cliente.getCli_cidade());
            pst.setString(6, cliente.getCli_estado());
            pst.setString(7, cliente.getCli_telfixo());
            pst.setString(8, cliente.getCli_celular());
            pst.setString(9, cliente.getCli_email());
            pst.setString(10, cliente.getCli_cpf_cnpj());
            pst.setString(11, cliente.getCli_cep());
            pst.setString(12, cliente.getCli_bairro());
            pst.setInt(13, cliente.getCli_tipo());
            pst.setInt(14, cliente.getCli_id());

            pst.execute();

            JOptionPane.showMessageDialog(jfcliente, "Cliente alterado com sucesso!");

        } catch (Exception e) {

            JOptionPane.showMessageDialog(jfcliente, "Erro ao alterar cliente: " + e);
        }

        Conexao.desconector(conexao);

    }
    
    //MÉTODO DE DELEÇÃO DE CLIENTE
    public void deletarCliente(Cliente cliente, JFrame jfcliente){
        
        String sql = "delete from Cliente where cli_id = ?";
        
        try {
            
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            
            pst.setInt(1, cliente.getCli_id());
            
            if(JOptionPane.showConfirmDialog(jfcliente, "Deseja excluir o registro?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION)==0){
                pst.execute();
                JOptionPane.showMessageDialog(jfcliente, "Registro excluído com sucesso!");
                
            }
            
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(jfcliente, "Erro ao excluir registro: "+e);
        }
        
        Conexao.desconector(conexao);
    }
    
    

}
