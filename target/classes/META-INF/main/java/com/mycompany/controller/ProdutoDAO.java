/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controller;
import com.mycompany.model.Produto;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author diegomoraes
 */
public class ProdutoDAO {
    
    //ATRIBUTOS DA CLASSE PRODUTODAO
    Connection conexao=null;
    PreparedStatement pst=null;
    ResultSet rs = null;
    
    //MÉTODOS DA CLASSE PRODUTODAO
    
    //MÉTODO PARA CONSULTAR TODOS OS REGISTROS
    public void consultarTodos(JTable tabproduto, JFrame jfproduto){
        
          String sql = "select prod_id as ID, prod_marca as Marca, prod_modelo as Modelo from Produto";
        
        try {
            conexao=Conexao.conectar();
            pst=conexao.prepareStatement(sql);
            rs=pst.executeQuery();
            
            tabproduto.setModel(DbUtils.resultSetToTableModel(rs));
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfproduto, "Ocorreu um erro: "+e);
        }
        
        Conexao.desconector(conexao);
        
    }
    
    //MÉTODO PARA CONSULTAR PRODUTOS POR NOME
    public void consultarNome(JTable tabprodutovenda, JFrame jfproduto, JTextField busca){
        
          String sql = "select prod_id as ID, prod_marca as Marca, prod_modelo as Modelo, prod_venda as ValorVenda from Produto "
                  + "where prod_modelo like ?";
        
        try {
            conexao=Conexao.conectar();
            pst=conexao.prepareStatement(sql);
            pst.setString(1, busca.getText() + "%");
            rs=pst.executeQuery();
            
            tabprodutovenda.setModel(DbUtils.resultSetToTableModel(rs));
            tabprodutovenda.getColumnModel().getColumn(0).setPreferredWidth(10);
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfproduto, "Ocorreu um erro: "+e);
        }
        
        Conexao.desconector(conexao);
        
    }
    
     //MÉTODO DE CONSULTA POR ID
    public ResultSet consultarId(int id, JFrame jfproduto) throws SQLException{
        
          String sql = "select * from Produto where prod_id="+id;
        
        try {
            conexao=Conexao.conectar();
            pst=conexao.prepareStatement(sql);
            rs=pst.executeQuery();
            
            
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfproduto, "Ocorreu um erro: "+e);
            
            
        }
        if(rs.next()){
             return rs;
        }else{
          return rs=null;  
        }
       
       // Conexao.desconector(conexao);
        
        
    }
    
    //MÉTODO DE INSERÇÃO NO BANCO
    public void inserirProduto(Produto produto, JFrame jfproduto){
        
         String sql = "insert into Produto(prod_marca, prod_modelo, prod_tipo, prod_descricao, prod_custo, prod_venda) "
                      + "values (?,?,?,?,?,?)";
         
         try {
             
            conexao=Conexao.conectar();
            pst=conexao.prepareStatement(sql);
            
            pst.setString(1, produto.getProd_marca());
            pst.setString(2, produto.getProd_modelo());
            pst.setInt(3, produto.getProd_tipo());
            pst.setString(4, produto.getProd_descricao());
            pst.setDouble(5, produto.getProd_custo());
            pst.setDouble(6, produto.getProd_venda());
            
            pst.execute();
            
            JOptionPane.showMessageDialog(jfproduto, "Produto inserido com sucesso!");
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfproduto, "Erro ao inserir produto: "+e);
        }
         
         Conexao.desconector(conexao);
        
        
    }
    //MÉTODO DE ALTERAÇÃO DO PRODUTO
    public void alterarProduto(Produto produto, JFrame jfproduto){
        
        String sql = "update Produto set prod_marca=?, prod_modelo=?, prod_tipo=?, prod_descricao=?, prod_custo=?, prod_venda=?"
                     + " where prod_id=?";
        
        try {
            
            conexao=Conexao.conectar();
            pst=conexao.prepareStatement(sql);
            
            pst.setString(1, produto.getProd_marca());
            pst.setString(2, produto.getProd_modelo());
            pst.setInt(3, produto.getProd_tipo());
            pst.setString(4, produto.getProd_descricao());
            pst.setDouble(5, produto.getProd_custo());
            pst.setDouble(6, produto.getProd_venda());
            pst.setInt(7, produto.getProd_id());
            
            pst.execute();
            
            JOptionPane.showMessageDialog(jfproduto, "Produto alterado com sucesso!");
            
            
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(jfproduto, "Erro ao alterar produto: "+e);
        }
        
        Conexao.desconector(conexao);
        
        
    }
    //MÉTODO DE DELEÇÃO DO PRODUTO
    public void deletarProduto(Produto produto, JFrame jfproduto){
        
        String sql = "delete from Produto where prod_id = ?";
        
        try {
            
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            
            pst.setInt(1, produto.getProd_id());
            
            if(JOptionPane.showConfirmDialog(jfproduto, "Deseja excluir o registro?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION)==0){
                pst.execute();
                JOptionPane.showMessageDialog(jfproduto, "Registro excluído com sucesso!");
                
            }
            
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(jfproduto, "Erro ao excluir registro: "+e);
        }
        
        Conexao.desconector(conexao);
    }
    
}
