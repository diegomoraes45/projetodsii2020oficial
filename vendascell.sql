-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 24/11/2020 às 01:08
-- Versão do servidor: 10.4.11-MariaDB
-- Versão do PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `vendascell`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `Carrinho`
--

CREATE TABLE `Carrinho` (
  `car_id` int(11) NOT NULL,
  `car_prod_id` int(11) NOT NULL,
  `car_prod_marca` varchar(80) NOT NULL,
  `car_prod_nome` varchar(100) NOT NULL,
  `car_prod_preco` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Cliente`
--

CREATE TABLE `Cliente` (
  `cli_id` int(11) NOT NULL,
  `cli_nome` varchar(50) NOT NULL,
  `cli_sobrenome` varchar(50) NOT NULL,
  `cli_datanasc` varchar(20) NOT NULL,
  `cli_endereco` varchar(200) NOT NULL,
  `cli_cidade` varchar(50) NOT NULL,
  `cli_estado` char(2) NOT NULL,
  `cli_telfixo` varchar(20) NOT NULL,
  `cli_celular` varchar(20) NOT NULL,
  `cli_email` varchar(150) DEFAULT NULL,
  `cli_cpf_cnpj` varchar(20) DEFAULT NULL,
  `cli_cep` varchar(9) DEFAULT NULL,
  `cli_bairro` varchar(100) NOT NULL,
  `cli_tipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `Cliente`
--

INSERT INTO `Cliente` (`cli_id`, `cli_nome`, `cli_sobrenome`, `cli_datanasc`, `cli_endereco`, `cli_cidade`, `cli_estado`, `cli_telfixo`, `cli_celular`, `cli_email`, `cli_cpf_cnpj`, `cli_cep`, `cli_bairro`, `cli_tipo`) VALUES
(14, 'd', 'd', '08/12/1985', 'Avenida Professora Maria de Lourdes Prado Cassetari', 'Botucatu', 'SP', '(11)1111-1111', '(11)11111-1111', 'd', '11111111111', '18609-110', 'Vila dos Lavradores', 0),
(15, 'Teste Diego', 'Teste Novo', '08/12/2000', 'Avenida Professora Maria de Lourdes Prado Cassetari', 'Botucatu', 'SP', '(11)1111-1111', '(11)11111-1111', 'diego@teste.com.br', '11111111111112', '18609-110', 'Vila dos Lavradores', 1),
(16, 'v', 'v', '12/10/1986', 'Avenida Professora Maria de Lourdes Prado Cassetari', 'Botucatu', 'SP', '(11)1111-1111', '(11)11111-1111', 'v', '35763714865', '18609-110', 'Vila dos Lavradores', 0),
(17, 'Diego', 'Moraes', '08/12/1985', 'Rua Erasmo Cunha Cezar 587', 'Botucatu', 'SP', '(11)1111-1111', '(14)99791-7575', 'die.gomoraes45@gmail.com', '35763714865', '18608-705', 'Jardim Ypê', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `ItemVenda`
--

CREATE TABLE `ItemVenda` (
  `item_id` int(11) NOT NULL,
  `Produto_prod_id` int(11) NOT NULL,
  `Venda_venda_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `ItemVenda`
--

INSERT INTO `ItemVenda` (`item_id`, `Produto_prod_id`, `Venda_venda_id`) VALUES
(2, 1, 8),
(3, 2, 8),
(4, 1, 8),
(5, 1, 9),
(6, 2, 10),
(7, 1, 10),
(8, 2, 10),
(9, 2, 11),
(10, 1, 11),
(11, 2, 11),
(12, 2, 12),
(13, 1, 12),
(14, 2, 12),
(15, 1, 12),
(16, 2, 12),
(17, 1, 12);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Produto`
--

CREATE TABLE `Produto` (
  `prod_id` int(11) NOT NULL,
  `prod_descricao` varchar(100) NOT NULL,
  `prod_modelo` varchar(100) NOT NULL,
  `prod_marca` varchar(100) NOT NULL,
  `prod_tipo` int(11) NOT NULL,
  `prod_custo` decimal(10,2) NOT NULL,
  `prod_venda` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `Produto`
--

INSERT INTO `Produto` (`prod_id`, `prod_descricao`, `prod_modelo`, `prod_marca`, `prod_tipo`, `prod_custo`, `prod_venda`) VALUES
(1, 'Celular teste', 'iPhone 8 Plus 256', 'Apple', 0, '1500.56', '5000.54'),
(2, 'Teste', 'S10 Note', 'Samsung', 0, '5000.34', '7023.54');

-- --------------------------------------------------------

--
-- Estrutura para tabela `Usuario`
--

CREATE TABLE `Usuario` (
  `usu_id` int(11) NOT NULL,
  `usu_nome` varchar(50) NOT NULL,
  `usu_senha` varchar(150) NOT NULL,
  `usu_perfil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `Usuario`
--

INSERT INTO `Usuario` (`usu_id`, `usu_nome`, `usu_senha`, `usu_perfil`) VALUES
(7, 'fulano', '202cb962ac59075b964b07152d234b70', 0),
(8, 'teste', '202cb962ac59075b964b07152d234b70', 0),
(10, 'Usuario Novo', '202cb962ac59075b964b07152d234b70', 0),
(11, 'Diego Moraes', '202cb962ac59075b964b07152d234b70', 0),
(17, 'teste', '202cb962ac59075b964b07152d234b70', 0),
(18, 'testenovo', '202cb962ac59075b964b07152d234b70', 1),
(19, 'teste', '202cb962ac59075b964b07152d234b70', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Venda`
--

CREATE TABLE `Venda` (
  `venda_id` int(11) NOT NULL,
  `venda_data` varchar(10) NOT NULL,
  `venda_total` decimal(10,2) NOT NULL,
  `Cliente_cli_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `Venda`
--

INSERT INTO `Venda` (`venda_id`, `venda_data`, `venda_total`, `Cliente_cli_id`) VALUES
(8, '21/11/2020', '17024.62', 15),
(9, '21/11/2020', '5000.54', 17),
(10, '23/11/2020', '19047.62', 17),
(11, '23/11/2020', '19047.62', 15),
(12, '23/11/2020', '36072.24', 17);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `vendasview`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `vendasview` (
`Venda_venda_id` int(11)
,`Produto_prod_id` int(11)
,`prod_id` int(11)
,`prod_descricao` varchar(100)
,`prod_modelo` varchar(100)
,`prod_marca` varchar(100)
,`prod_tipo` int(11)
,`prod_custo` decimal(10,2)
,`prod_venda` decimal(10,2)
,`venda_id` int(11)
,`venda_data` varchar(10)
,`venda_total` decimal(10,2)
,`Cliente_cli_id` int(11)
);

-- --------------------------------------------------------

--
-- Estrutura para view `vendasview`
--
DROP TABLE IF EXISTS `vendasview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vendasview`  AS  select `I`.`Venda_venda_id` AS `Venda_venda_id`,`I`.`Produto_prod_id` AS `Produto_prod_id`,`P`.`prod_id` AS `prod_id`,`P`.`prod_descricao` AS `prod_descricao`,`P`.`prod_modelo` AS `prod_modelo`,`P`.`prod_marca` AS `prod_marca`,`P`.`prod_tipo` AS `prod_tipo`,`P`.`prod_custo` AS `prod_custo`,`P`.`prod_venda` AS `prod_venda`,`V`.`venda_id` AS `venda_id`,`V`.`venda_data` AS `venda_data`,`V`.`venda_total` AS `venda_total`,`V`.`Cliente_cli_id` AS `Cliente_cli_id` from ((`itemvenda` `I` join `produto` `P`) join `venda` `V`) where `I`.`Venda_venda_id` = `V`.`venda_id` and `I`.`Produto_prod_id` = `P`.`prod_id` ;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `Carrinho`
--
ALTER TABLE `Carrinho`
  ADD PRIMARY KEY (`car_id`);

--
-- Índices de tabela `Cliente`
--
ALTER TABLE `Cliente`
  ADD PRIMARY KEY (`cli_id`);

--
-- Índices de tabela `ItemVenda`
--
ALTER TABLE `ItemVenda`
  ADD PRIMARY KEY (`item_id`,`Produto_prod_id`,`Venda_venda_id`),
  ADD KEY `fk_Produto_has_Venda_Venda1_idx` (`Venda_venda_id`),
  ADD KEY `fk_Produto_has_Venda_Produto1_idx` (`Produto_prod_id`);

--
-- Índices de tabela `Produto`
--
ALTER TABLE `Produto`
  ADD PRIMARY KEY (`prod_id`);

--
-- Índices de tabela `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`usu_id`);

--
-- Índices de tabela `Venda`
--
ALTER TABLE `Venda`
  ADD PRIMARY KEY (`venda_id`) USING BTREE,
  ADD KEY `fk_Venda_Cliente_idx` (`Cliente_cli_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `Carrinho`
--
ALTER TABLE `Carrinho`
  MODIFY `car_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Cliente`
--
ALTER TABLE `Cliente`
  MODIFY `cli_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de tabela `ItemVenda`
--
ALTER TABLE `ItemVenda`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de tabela `Produto`
--
ALTER TABLE `Produto`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `usu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de tabela `Venda`
--
ALTER TABLE `Venda`
  MODIFY `venda_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `ItemVenda`
--
ALTER TABLE `ItemVenda`
  ADD CONSTRAINT `fk_Produto_has_Venda_Pedido1` FOREIGN KEY (`Venda_venda_id`) REFERENCES `Venda` (`venda_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Produto_has_Venda_Produto1` FOREIGN KEY (`Produto_prod_id`) REFERENCES `Produto` (`prod_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `Venda`
--
ALTER TABLE `Venda`
  ADD CONSTRAINT `fk_Venda_Cliente` FOREIGN KEY (`Cliente_cli_id`) REFERENCES `Cliente` (`cli_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
