/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

/**
 *
 * @author diegomoraes
 */
public class Venda {
    
    private int venda_id;
    private int Cliente_cli_id;
    private String venda_data;
    private double venda_total;

    /**
     * @return the venda_id
     */
    public int getVenda_id() {
        return venda_id;
    }

    /**
     * @param venda_id the venda_id to set
     */
    public void setVenda_id(int venda_id) {
        this.venda_id = venda_id;
    }

    /**
     * @return the Cliente_cli_id
     */
    public int getCliente_cli_id() {
        return Cliente_cli_id;
    }

    /**
     * @param Cliente_cli_id the Cliente_cli_id to set
     */
    public void setCliente_cli_id(int Cliente_cli_id) {
        this.Cliente_cli_id = Cliente_cli_id;
    }

    /**
     * @return the venda_data
     */
    public String getVenda_data() {
        return venda_data;
    }

    /**
     * @param venda_data the venda_data to set
     */
    public void setVenda_data(String venda_data) {
        this.venda_data = venda_data;
    }

    /**
     * @return the venda_total
     */
    public double getVenda_total() {
        return venda_total;
    }

    /**
     * @param venda_total the venda_total to set
     */
    public void setVenda_total(double venda_total) {
        this.venda_total = venda_total;
    }
    
    
    
    
    
}
