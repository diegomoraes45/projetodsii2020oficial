/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

/**
 *
 * @author diegomoraes
 */
public class Carrinho {
    
    private int car_id;
    private int car_prod_id;
    private String car_prod_nome;
    private String car_prod_marca;
    private double car_prod_preco;

    /**
     * @return the car_prod_marca
     */
    public String getCar_prod_marca() {
        return car_prod_marca;
    }

    /**
     * @param car_prod_marca the car_prod_marca to set
     */
    public void setCar_prod_marca(String car_prod_marca) {
        this.car_prod_marca = car_prod_marca;
    }
    
    /**
     * @return the car_id
     */
    public int getCar_id() {
        return car_id;
    }

    /**
     * @param car_id the car_id to set
     */
    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    /**
     * @return the car_prod_id
     */
    public int getCar_prod_id() {
        return car_prod_id;
    }

    /**
     * @param car_prod_id the car_prod_id to set
     */
    public void setCar_prod_id(int car_prod_id) {
        this.car_prod_id = car_prod_id;
    }

    /**
     * @return the car_prod_nome
     */
    public String getCar_prod_nome() {
        return car_prod_nome;
    }

    /**
     * @param car_prod_nome the car_prod_nome to set
     */
    public void setCar_prod_nome(String car_prod_nome) {
        this.car_prod_nome = car_prod_nome;
    }

    /**
     * @return the car_prod_preco
     */
    public double getCar_prod_preco() {
        return car_prod_preco;
    }

    /**
     * @param car_prod_preco the car_prod_preco to set
     */
    public void setCar_prod_preco(double car_prod_preco) {
        this.car_prod_preco = car_prod_preco;
    }
    
    
    
    
}
