/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controller;

import com.mycompany.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 *
 * @author diegomoraes
 */
public class LoginDAO {
    
    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
    
    String sql = "select * from Usuario where usu_nome = ? and usu_senha = md5(?)";
       
    
    public void acessaSistema(Usuario usu, JFrame jfprin, JFrame jflogin){
        
        try {
            
        conexao = Conexao.conectar();        
        pst = conexao.prepareStatement(sql);
        
        pst.setString(1, usu.getUsu_nome());
        pst.setString(2, usu.getUsu_senha());
        
        rs=pst.executeQuery();
        
        if(rs.next()){
            JOptionPane.showMessageDialog(jflogin, "Acesso Permitido");
            jfprin.setVisible(true);
            jflogin.setVisible(false);
            Conexao.desconector(conexao);
            
        }else{
             JOptionPane.showMessageDialog(jflogin, "Acesso Negado");
             Conexao.desconector(conexao);
            
        }
            
        } catch (Exception e) {
            
           JOptionPane.showMessageDialog(jflogin, "Ocorreu um erro: "+e);
           Conexao.desconector(conexao);
            
        }
        
        
        
        
        
        
    }
          
    
}
