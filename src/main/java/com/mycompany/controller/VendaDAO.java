/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controller;

import com.mycompany.model.Venda;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author diegomoraes
 */
public class VendaDAO {
    
    Connection conexao = null;
    PreparedStatement pst, pst2 = null;
    ResultSet rs, rs2 = null;
    
    
    //MÉTODO PARA CONSULTA DE PEDIDO E VERIFICAÇÃO DE ID DO ÚLTIMO REGISTRO
    public int consultarPedidoInicial(JFrame jfvenda) {

        String sql = "Select * from Venda";
        int codped=0;
        
        try {
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            if (rs.next()) {
                
                rs.last();
                codped = (rs.getInt("venda_id"))+1;

            }else{
                codped = 1;
            }

            Conexao.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }
         return codped;
    }
    
     //MÉTODO PARA INSERÇÃO DE PEDIDO
    public void inserirPed(Venda venda, JFrame jfvenda) {

        String sql = "insert into Venda (venda_data, Cliente_cli_id, venda_total) values (?,?,?) ";

        try {
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, venda.getVenda_data());
            pst.setInt(2, venda.getCliente_cli_id());
            pst.setDouble(3, venda.getVenda_total());
            
            pst.execute();

            Conexao.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao inserir: " + ex);

        }

    }
    
    //MÉTODO PARA TRANSFERIR ITEM DO CARRINHO PARA O PEDIDO
    public void inserirItemPedido(JFrame jfvenda, int codpedido) {

        String sql = "select * from Carrinho";
        String sql2 = "insert into ItemVenda (Produto_prod_id, Venda_venda_id) values (?,?) ";

        try {
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();
            
           
              while (rs.next()) {

                pst2 = conexao.prepareStatement(sql2);
                pst2.setInt(1, rs.getInt("car_prod_id"));
                pst2.setInt(2, codpedido);
                pst2.execute();

            }   

              Conexao.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao inserir: " + ex);

        }

    }
    
    //MÉTODO PARA CONSULTAR ITENS DE UM PEDIDO 
    public void consultarPedidoFinal(JTable tbConsultaVenda, JFrame jfvenda, int codped) {

        String sql = "select prod_marca as Marca, prod_modelo as Modelo, prod_venda as Preco from vendasview where venda_id = ?";

        try {
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codped);
            rs = pst.executeQuery();

            tbConsultaVenda.setModel(DbUtils.resultSetToTableModel(rs));
            Conexao.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }
    
    //MÉTODO PARA CONSULTAR TODOS OS PEDIDOS
    public void consultarVendaAll(JTable tbConsultaVenda, JFrame jfvenda) {

       String sql = "Select venda_id as ID, venda_data as Data, venda_total as Total from Venda";

        try {
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            tbConsultaVenda.setModel(DbUtils.resultSetToTableModel(rs));
            Conexao.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }
    
    
}
