/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controller;
import com.mycompany.model.Usuario;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author diegomoraes
 */
public class UsuarioDAO {
    
    //Atributos da Classe UsuarioDAO
    Connection conexao=null;
    PreparedStatement pst=null;
    ResultSet rs = null;
    
    //Métodos da Classe UsuarioDAO
    
    //Método de Consultar Todos os Registros
    public void consultarTodos(JTable tabusuario, JFrame jfusuario){
        
          String sql = "select usu_id as ID, usu_nome as Nome, usu_perfil as Perfil from Usuario";
        
        try {
            conexao=Conexao.conectar();
            pst=conexao.prepareStatement(sql);
            rs=pst.executeQuery();
            
            tabusuario.setModel(DbUtils.resultSetToTableModel(rs));
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfusuario, "Ocorreu um erro: "+e);
        }
        
        Conexao.desconector(conexao);
        
    }
    
    //Método de Inserção de Usuário no Banco
    public void inserirUsuario(Usuario usuario, JFrame jfusuario){
        
         String sql = "insert into Usuario(usu_nome, usu_senha, usu_perfil) values (?,md5(?),?)";
         
         try {
             
            conexao=Conexao.conectar();
            pst=conexao.prepareStatement(sql);
            
            pst.setString(1, usuario.getUsu_nome());
            pst.setString(2, usuario.getUsu_senha());
            pst.setInt(3, usuario.getUsu_perfil());
            
            pst.execute();
            
            JOptionPane.showMessageDialog(jfusuario, "Usuário inserido com sucesso!");
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfusuario, "Erro ao inserir usuário: "+e);
        }
         
         Conexao.desconector(conexao);
        
        
    }
    //Alteraçao do Usuário
    public void alterarUsuario(Usuario usuario, JFrame jfusuario){
        
        String sql = "update Usuario set usu_nome=?, usu_senha=md5(?), usu_perfil=? where usu_id=?";
        
        try {
            
            conexao=Conexao.conectar();
            pst=conexao.prepareStatement(sql);
            
            pst.setString(1, usuario.getUsu_nome());
            pst.setString(2, usuario.getUsu_senha());
            pst.setInt(3, usuario.getUsu_perfil());
            pst.setInt(4, usuario.getUsu_id());
            
            pst.execute();
            
            JOptionPane.showMessageDialog(jfusuario, "Usuário alterado com sucesso!");
            
            
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(jfusuario, "Erro ao alterar usuário: "+e);
        }
        
        Conexao.desconector(conexao);
        
        
    }
    
    public void deletarUsuario(Usuario usuario, JFrame jfusuario){
        
        String sql = "delete from Usuario where usu_id = ?";
        
        try {
            
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            
            pst.setInt(1, usuario.getUsu_id());
            
            if(JOptionPane.showConfirmDialog(jfusuario, "Deseja excluir o registro?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION)==0){
                pst.execute();
                JOptionPane.showMessageDialog(jfusuario, "Registro excluído com sucesso!");
                
            }
            
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(jfusuario, "Erro ao excluir registro: "+e);
        }
        
        Conexao.desconector(conexao);
    }
    
}
