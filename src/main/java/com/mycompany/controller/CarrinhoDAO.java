/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controller;

import com.mycompany.model.Carrinho;
import com.mycompany.model.Produto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author diegomoraes
 */
public class CarrinhoDAO {
    
    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
    
     //MÉTODO PARA INSERIR ITEM NO CARRINHO
    public void inserir(Produto produto, JFrame jfvenda) {

        String sql = "insert into Carrinho (car_prod_id, car_prod_marca, car_prod_nome, car_prod_preco) values (?,?,?,?) ";

        try {
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, produto.getProd_id());
            pst.setString(2, produto.getProd_marca());
            pst.setString(3, produto.getProd_modelo());
            pst.setDouble(4, produto.getProd_venda());

            pst.execute();

            Conexao.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao inserir: " + ex);

        }

    }
    
     //MÉTODO PARA CONSULTA DE ITENS NO CARRINHO
    public void consultarItensVenda(JTable tbItens, JFrame jfvenda) {

        String sql = "Select car_id as ID, car_prod_marca as Marca, car_prod_nome as Nome, car_prod_preco as Preco from Carrinho";

        try {
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            tbItens.setModel(DbUtils.resultSetToTableModel(rs));
            Conexao.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }
    
    //EXCLUI TODOS OS REGISTROS DO CARRINHO QUANDO FINALIZAR COMPRA OU SAIR
    public void limparCarrinho(JFrame jfvenda) {

        String sql = "Truncate table Carrinho";

        try {
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            pst.execute();

            Conexao.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }
    
    //MÉTODO PARA CALCULAR O VALOR TOTAL DE ITENS NO CARRINHO
    public Float totalCarrinho(JFrame jfvenda){
        
         String sql = "SELECT SUM(car_prod_preco) as total from Carrinho;";
         float total=0;
         float totaldec=0;
        try {
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            rs=pst.executeQuery();
            rs.first();
            totaldec = rs.getFloat("total");
            
            total = totaldec;
                        
            Conexao.desconector(conexao);
            
           

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }
        
        return total;
    }
    
    //MÉTODO PARA DELETAR ITEM DO CARRINHO
    public void deletar(Carrinho car, JFrame jfvenda) {

        String sql = "delete from Carrinho where car_id = ?";

        try {
            conexao = Conexao.conectar();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, car.getCar_id());

            if (JOptionPane.showConfirmDialog(jfvenda, "Deseja deletar?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION) == 0) {
                pst.execute();
                JOptionPane.showMessageDialog(jfvenda, "Deletado com sucesso!");
                Conexao.desconector(conexao);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao deletar: " + e);

        }

    }
    
    
}
