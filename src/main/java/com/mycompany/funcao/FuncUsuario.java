/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.funcao;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author diegomoraes
 */
public class FuncUsuario {

//Limpar Campos
    public void limparCampos(JTextField id,  JTextField nome, JPasswordField senha, JPasswordField senhar, JComboBox perfil) {
        id.setText(null);
        nome.setText(null);
        senha.setText(null);
        senhar.setText(null);
        perfil.setSelectedIndex(0);
    }

//Validação de Campos Vazios
    public boolean verificarCampos(JTextField nome, JPasswordField senha, JPasswordField senhar, JFrame jfusuario) {

        if (nome.getText().isEmpty()) {
            JOptionPane.showMessageDialog(jfusuario, "Por favor, preencha o campo Nome");
            nome.requestFocus();
            return false;
        } else {
            if (senha.getText().isEmpty()) {
                JOptionPane.showMessageDialog(jfusuario, "Por favor, preencha o campo Senha");
                senha.requestFocus();
                return false;
            }else{
                if(senhar.getText().isEmpty()){
                    JOptionPane.showMessageDialog(jfusuario, "Por favor, redigite a Senha");
                    senhar.requestFocus();
                    return false;
                }
            }
        }
        
        return true;

    }
    
    
//Enviar dados da JTable para tab de manutenção de usuários
    public void enviarDados(JTable tabUsuario, JTextField id, JTextField nome, JComboBox perfil, JTabbedPane pagina){
        
        int linha = tabUsuario.getSelectedRow();
        
        id.setText(tabUsuario.getModel().getValueAt(linha, 0).toString());
        nome.setText(tabUsuario.getModel().getValueAt(linha, 1).toString());
        perfil.setSelectedIndex((int)tabUsuario.getModel().getValueAt(linha, 2));
        
        pagina.setSelectedIndex(1);
        
        
    }

//Habilitar Campos
    public void habilitarCampos(JTextField nome, JPasswordField senha, JPasswordField senhar, JComboBox perfil) {

        nome.setEnabled(true);
        senha.setEnabled(true);
        senhar.setEnabled(true);
        perfil.setEnabled(true);

    }

//Desabilitar Campos
    public void desabilitarCampos(JTextField nome, JPasswordField senha, JPasswordField senhar, JComboBox perfil) {

        nome.setEnabled(false);
        senha.setEnabled(false);
        senhar.setEnabled(false);
        perfil.setEnabled(false);

    }

//Botões - Ação de Inserção
    public void acaoCUD(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar, JTextField usuario) {

        inserir.setEnabled(false);
        editar.setEnabled(false);
        deletar.setEnabled(false);
        ok.setEnabled(true);
        cancelar.setEnabled(true);
        usuario.requestFocus();

    }

//Botões - Ação de Ir para tela de Edição
    public void acaoIrEdicao(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar) {

        inserir.setEnabled(false);
        editar.setEnabled(true);
        deletar.setEnabled(true);
        ok.setEnabled(false);
        cancelar.setEnabled(false);

    }

//Botões - Ação de Edição
    public void acaoEdicao(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar) {

        inserir.setEnabled(false);
        editar.setEnabled(false);
        deletar.setEnabled(false);
        ok.setEnabled(true);
        cancelar.setEnabled(true);

    }

//Botões - Ação de OK e Cancelar
    public void acaoOkCancelar(JButton inserir, JButton editar, JButton deletar, JButton ok, JButton cancelar) {

        inserir.setEnabled(true);
        editar.setEnabled(false);
        deletar.setEnabled(false);
        ok.setEnabled(false);
        cancelar.setEnabled(false);

    }

}
