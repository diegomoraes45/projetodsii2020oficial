/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.funcao;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author diegomoraes
 */
public class BuscaCEP {
    
    public void buscarCEP(JTextField cep, JTextField logradouro, JTextField bairro, JTextField cidade, JTextField estado, JFrame jf){
        
         
        
        try {
            String valor = cep.getText();      
            
            br.com.correios.bsb.sigep.master.bean.cliente.AtendeClienteService service = new br.com.correios.bsb.sigep.master.bean.cliente.AtendeClienteService();
            br.com.correios.bsb.sigep.master.bean.cliente.AtendeCliente port = service.getAtendeClientePort();
            br.com.correios.bsb.sigep.master.bean.cliente.EnderecoERP result = port.consultaCEP(valor);
            logradouro.setText(result.getEnd());
            bairro.setText(result.getBairro());
            cidade.setText(result.getCidade());
            estado.setText(result.getUf()); 
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(jf, "O CEP informado é inválido ou não foi encontrado!");
        } 
        
    }
    
}
