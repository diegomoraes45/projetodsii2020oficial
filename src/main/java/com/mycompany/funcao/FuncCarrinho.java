/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.funcao;

import com.mycompany.controller.CarrinhoDAO;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author diegomoraes
 */
public class FuncCarrinho {
    
    
    
    //FUNÇÃO PARA CONVERSÃO DO FORMATO DE MOEDA 
    public void converteMoeda(String total, CarrinhoDAO carDAO, JFrame jVenda, JLabel lbl_total){
                
        DecimalFormat f = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
        total=String.valueOf(f.format(carDAO.totalCarrinho(jVenda)));
        lbl_total.setText("R$"+total); 
        
    }
    
}
